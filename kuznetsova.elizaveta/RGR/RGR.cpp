﻿#include <iostream>
#include "FrequencyDictionary.h"
#include <fstream>
#include <string>
#include <sstream>
#include <filesystem>
#include <cctype>


bool isWord(std::string& word);
bool isBackBracket(char& symbol);
bool isFrontBracket(char& symbol);
bool isPunctuationSymbol(char& symbol);
bool isFilename(std::string& word);


bool isWord(std::string& word)
{
    if (word.size() == 0)
    {
        return false;
    }
    for (size_t i = 0; i < word.size(); i++)
    {
        if (!(std::isalpha(word[i])))
        {
            if ((i == (word.size() - 1) && (isBackBracket(word[i]) || isPunctuationSymbol(word[i]))) || (i == 0 && isFrontBracket(word[i])))
            {
                word.erase(i, 1);
                if (word.size() == 0)
                {
                    return false;
                }
                i--;
                continue;
            }
            else if ((i == word.size() - 2) && isBackBracket(word[i]) && isPunctuationSymbol(word[i + 1]))
            {
                word.erase(i, 2);
                continue;
            }
            return false;
        }
        word[i] = tolower(word[i]);
    }
    return true;
}

bool isPunctuationSymbol(char& symbol)
{
    return symbol == '.' || symbol == ',' || symbol == ';' || symbol == ':' || symbol == '!' || symbol == '?';
}

bool isBackBracket(char& symbol)
{
    return symbol == ')' || symbol == '}' || symbol == ']' || symbol == '"';
}
bool isFrontBracket(char& symbol)
{
    return symbol == '(' || symbol == '{' || symbol == '[' || symbol == '"';
}
bool isFilename(std::string& word)
{
    if (word.size() < 4)
    {
        return false;
    }
    return word.substr(word.size() - 4) == ".txt";
}

int main()
{
    FrequencyDictionary dictionary;
    std::string command;
    std::string word;
    std::string text;
    std::string filename;
    std::istringstream iss1("INSERTFROMFILE Test.txt \n\
                            SEARCH sed\n\
                            SEARCH apple\n\
                            REMOVE sed\n\
                            SEARCH sed\n\
                            TOP3\n\
                            INSERT hi hi hi hi hi hi hi hi hi hi\n\
                            TOP3\n\
                            ghjs\n\
                            SEARCH 6839\n\
                            REMOVE gjk");
    while (!iss1.eof())
    {
        std::getline(iss1, text);
        std::istringstream iss(text);
        iss >> command;
        if (command == "SEARCH" || command == "INSERT" || command == "REMOVE" || command == "TOP3" || command == "INSERTFROMFILE")
        {
            if (command == "INSERT")
            {
                size_t count = 0;
                while (!iss.eof())
                {
                    iss >> word;
                    if (isWord(word))
                    {
                        if (dictionary.insertWord(word))
                        {
                            count++;
                        }
                    }
                }
                std::cout << "The number of words successfully read from your text is " << count << '\n';
            }
            else if (command == "SEARCH")
            {
                iss >> word;
                if (isWord(word))
                {
                    if (dictionary.searchWord(word) == 0)
                    {
                        std::cout << "<NO SUCH WORD IN DICTIONARY>\n";
                    }
                    else
                    {
                        std::cout << "The word \"" << word << "\" occured " << dictionary.searchWord(word) << " times\n";
                    }
                }
                else
                {
                    std::cout << "<INVALID ARGUMENT FOR COMMAND>\n";
                }
            }
            else if (command == "REMOVE")
            {
                iss >> word;
                if (isWord(word))
                {
                    if (dictionary.removeWord(word))
                    {
                        std::cout << "The word \"" << word << "\" was deleted from the dictionary\n";
                    }
                    else
                    {
                        std::cout << "<NO SUCH WORD IN DICTIONARY>\n";
                    }
                }
                else
                {
                    std::cout << "<INVALID ARGUMENT FOR COMMAND>\n";
                }
            }
            else if (command == "TOP3")
            {
                dictionary.printMostFrequentWords();
            }
            else if (command == "INSERTFROMFILE")
            {
                iss >> filename;
                std::ifstream inputFile(filename);
                if (!inputFile.is_open())
                {
                    std::cerr << "<FILE OPENING ERROR>\n";
                    iss.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                    continue;
                }
                else
                {
                    size_t count = 0;
                    while (!inputFile.eof())
                    {
                        inputFile >> word;
                        if (isWord(word))
                        {
                            if (dictionary.insertWord(word))
                            {
                                count++;
                            }
                        }
                    }
                    inputFile.close();
                    std::cout << "The number of words successfully read from your text is " << count << '\n';
                    dictionary.printMostFrequentWords();
                }
            }
        }
        else
        {
            std::cout << "<INVALID COMMAND>\n";
            iss.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
    }
return 0;
}