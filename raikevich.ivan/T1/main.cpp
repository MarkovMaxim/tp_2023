#include <fstream>
#include "Header.hpp"

int main() {
    std::ifstream inputFile("input.txt");
    if (!inputFile) {
        std::cerr << "Error opening input file." << std::endl;
        return -1;
    }
    std::vector<DataStruct> vec;
    std::copy(std::istream_iterator<DataStruct>(inputFile), std::istream_iterator<DataStruct>(), std::back_inserter(vec));
    inputFile.close();
    std::sort(vec.begin(), vec.end());
    std::copy(vec.begin(), vec.end(), std::ostream_iterator<DataStruct>(std::cout, "\n"));

    return 0;
}