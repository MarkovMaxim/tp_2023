﻿#include <iostream>
#include <map>
#include <string>
#include <iostream>
#include <sstream>

/// @brief Печать данных в контейнере map
void PrintDataStruct(const std::map<std::string, std::string>& dataStruct) {
    for (const auto& pair : dataStruct) {
        std::cout << pair.first << ": " << pair.second << std::endl;
    }
}

enum class Menu
{
    Add = 1,
    Remove = 2,
    Find = 3,
    Show = 4,
    Exit = 5
};

int GetElementConsole(std::istringstream& iss) {
    int value;
    iss >> value;
    return value;
}


int main() {
    std::map<std::string, std::string> myMap;

    std::string key;
    std::string value;
    bool isValueForPrint = false;
    std::string input_string = "1\ncar\nмашина\n4\n\n3\ncar\n2\ncar\n4\n\n5\n";
    std::istringstream iss(input_string);

    for (;;) {
        if (isValueForPrint) {
            std::cout << " /-----------------Hash_Table-----------------\\\n";
            PrintDataStruct(myMap);
        }

        std::cout << "+=========================Menu=========================+\n"
            << "1 - Add\n"
            << "2 - Remove\n"
            << "3 - Find\n"
            << "4 - Show\n"
            << "5 - Exit\n";
        const int valueForMenu = GetElementConsole(iss);

        switch (static_cast<Menu>(valueForMenu)) {
        case Menu::Add: {
            std::cout << "Key: ";
            iss >> key;
            std::cout << key << "\n";
            std::cout << "Value: ";
            iss >> value;
            std::cout << value << "\n";
            myMap[key] = value;
            break;
        }

        case Menu::Remove: {
            std::cout << "Key: ";
            iss >> key;
            std::cout << key << "\n";
            auto it = myMap.find(key);
            if (it != myMap.end()) {
                myMap.erase(it);
                std::cout << "Item removed successfully." << std::endl;
            }
            else {
                std::cout << "Item not found." << std::endl;
            }
            break;
        }

        case Menu::Find: {
            std::cout << "Key: ";
            iss >> key;
            std::cout << key << "\n";
            auto it = myMap.find(key);
            if (it != myMap.end()) {
                std::cout << "Value: " << it->second << std::endl;
            }
            else {
                std::cout << "Item not found." << std::endl;
            }
            break;
        }

        case Menu::Show: {
            isValueForPrint = true;
            break;
        }

        case Menu::Exit: {
            return 0;
        }

        default: {
            std::cout << "Invalid Value\n";
            break;
        }
        }
        system("cls");
    }
}
