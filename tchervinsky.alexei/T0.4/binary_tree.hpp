// 2023-04-04 alexeit at home 2:35
#ifndef BINARY_TREE_HPP
#define BINARY_TREE_HPP

#include <iostream>

template <typename T>
class BinarySearchTree
{
public:  
  BinarySearchTree();
  BinarySearchTree(const BinarySearchTree<T> & ) = delete;
  BinarySearchTree(BinarySearchTree<T> && other) noexcept;
  BinarySearchTree<T> & operator = (const BinarySearchTree<T> & ) = delete;
  BinarySearchTree<T> & operator = (BinarySearchTree<T> && other) noexcept;
  virtual ~BinarySearchTree();

  // 1.1 
  bool iterativeSearch(const T & key) const noexcept;
  // 2
  bool insert(const T & key);
   // 4.1
  void print(std::ostream & out) const noexcept;

private:
  template <typename U> struct Node;
  // 1.2
  Node<T> * iterativeSearchNode(const T & key) const noexcept;

  // 4.2
  void printNode(std::ostream & out, Node<T> * node) const noexcept;

  // clean
  void clean(Node<T> * node);

  template <typename U>
  struct Node
  {
    T key_;
    Node<T> * left_;
    Node<T> * right_;
    Node<T> * p_;
    explicit Node(T key, Node * left = nullptr, Node * right = nullptr, Node * p = nullptr) :
      key_(key),
      left_(left),
      right_(right),
      p_(p)  
    {
    }
  };
  Node<T> * root_;
};

template <typename T>
BinarySearchTree<T>::BinarySearchTree() :
  root_(nullptr)
{
}

template <typename T>
BinarySearchTree<T>::BinarySearchTree(BinarySearchTree<T> && other) noexcept :
  root_(other.root_)
{
  other.root_ = nullptr;
}  

template <typename T>
BinarySearchTree<T> & BinarySearchTree<T>::operator = (BinarySearchTree<T> && other) noexcept 
{ 
  if (*this != other)
  {
    root_ = other.root_;
    other.root_ = nullptr;
  }
  return *this;
}

template <typename T>
BinarySearchTree<T>::~BinarySearchTree()
{
  clean(root_);
}

// 1.2
template <typename T>
bool BinarySearchTree<T>::iterativeSearch(const T & key) const noexcept
{
  return (iterativeSearchNode(key) ? true : false);
}

// 4.1
template <typename T>
void BinarySearchTree<T>::print(std::ostream & out) const noexcept
{
  printNode(out, root_);
  out << '\n';
}

// PRIVATE

template <typename T>
bool BinarySearchTree<T>::insert(const T & key)
{
  if (iterativeSearch(key))
  {
    return false;
  }
  Node<T> * z = new Node<T>(key);

  Node<T> * x = root_;
  Node<T> * y = nullptr;

  while(x)
  {
    y = x;
    if (key < x->key_)
    {
      x = x->left_;
    }
    else
    {
      x = x->right_;
    }
  }

  z->p_ = y;
  
  if (y == nullptr)
  {
    root_ = z;
  }
  else if (z->key_ < y->key_)
  {
    y->left_ = z;
  } 
  else
  {
    y->right_ = z;
  }
  return true;
}


template <typename T>
typename BinarySearchTree<T>::template Node<T> * BinarySearchTree<T>::iterativeSearchNode(const T & key) const noexcept
{
  Node<T> * x = root_;
  while(x)
  {
    if (x->key_ == key)
    {
      return x;
    }
    else if (x->key_ > key)
    {
      x = x->left_;
    }
    else
    {
      x = x->right_;
    }
  }
  return x;
}

// 4.2
template <typename T>
void BinarySearchTree<T>::printNode(std::ostream & out, BinarySearchTree<T>::Node<T> * node) const noexcept
{
  if (node == nullptr)
  {    
    return;
  }
  out << '(';
  out << node->key_;
  printNode(out, node->left_);
  printNode(out, node->right_);
  out << ')';
  return;
}

// clean
template <typename T>
void BinarySearchTree<T>::clean(BinarySearchTree<T>::Node<T> * node)
{
  if (node == nullptr)
  {
    return;
  }
  else
  {
    clean(node->left_);
    clean(node->right_);
  }
  delete node;
  return;
}

#endif // BINARY_TREE_HPP
