﻿#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <sstream>

// Определение структуры DataStruct
struct DataStruct
{
    unsigned long long key1;
    long long key2;
    std::string key3;
};

// Определение оператора >> для чтения DataStruct из потока
std::istream& operator>>(std::istream& is, DataStruct& data)
{
    char c;
    is >> c; // Читаем открывающую скобку

    // Читаем поля структуры
    while (is >> c && c != ')')
    {
        std::string key_name;
        is >> key_name >> std::ws; // Читаем имя поля

        if (key_name == "key1")
        {
            is >> std::hex >> data.key1; // Читаем беззнаковое значение key1 в шестнадцатеричном формате
        }
        else if (key_name == "key2")
        {
            is >> data.key2; // Читаем знаковое значение key2
        }
        else if (key_name == "key3")
        {
            std::getline(is, data.key3, ':'); // Читаем строковое значение key3
        }
        else
        {
            // Неизвестное имя поля - игнорируем строку
            std::string unused;
            std::getline(is, unused);
            break;
        }

        is >> c; // Читаем символ ':' после значения поля
    }

    return is;
}

// Определение оператора << для вывода DataStruct в поток
std::ostream& operator<<(std::ostream& os, const DataStruct& data)
{
    os << "(:key1 " << std::hex << data.key1 << ":"
        << "key2 " << data.key2 << ":"
        << "key3 \"" << data.key3 << "\":)";

    return os;
}

// Функция для сравнения DataStruct по требуемым полям
bool CompareDataStruct(const DataStruct& lhs, const DataStruct& rhs)
{
    if (lhs.key1 != rhs.key1)
    {
        return lhs.key1 < rhs.key1; // Сортировка по возрастанию key1
    }
    else if (lhs.key2 != rhs.key2)
    {
        return lhs.key2 < rhs.key2; // Сортировка по возрастанию key2, если key1 одинаковые
    }
    else if (lhs.key3.length() != rhs.key3.length())
    {
        return lhs.key3.length() < rhs.key3.length(); // Сортировка по возрастанию длины строки key3, если прочие поля равны
    }
    else
    {
        return lhs.key3 < rhs.key3; // Сортировка по возрастанию key3, если прочие поля равны и длины строк key3 равны
    }
}

int main()
{
    // Входные данные для инициализации std::vector<DataStruct>
    std::istringstream iss("(:key1 10ull:key2 'c':key3 \"Data\":)\n(:key3 \"Data\":key2 'c':key1 10ull:)\n(:key2 -89LL:key3 \"Data\":key1 89ll:)");

    // Создание std::vector<DataStruct> с помощью итераторов и алгоритмов STL
    std::vector<DataStruct> data_vector;
    std::copy(std::istream_iterator<DataStruct>(iss >> std::ws), std::istream_iterator<DataStruct>(), std::back_inserter(data_vector));

    // Сортировка данных
    std::sort(data_vector.begin(), data_vector.end(), CompareDataStruct);

    // Вывод отсортированных данных
    std::ostream_iterator<DataStruct> out_it(std::cout, "\n");
    std::copy(data_vector.begin(), data_vector.end(), out_it);

    return 0;
}