#include "Polygon.h"

int main()
{
    std::string input_string = "3 (1;1) (1;3) (3;3)\n3 (1;1) (1;3) (3;3)\n3 (1;1) (1;3) (3;3)\n4 (0;0) (2;1) (2;0) (0;1)\n5 (0;0) (0;1) (1;2) (2;1) (2;0)";
    std::istringstream iss(input_string);

    std::vector<Polygon> polygons;
    while (iss.peek() != EOF)
    {
        Polygon polygon;
        iss >> polygon;
        polygons.push_back(polygon);
    }

    std::string commands = "PRINTENTER\nECHO 3 (1;1) (1;3) (3;3)\nPRINTENTER\nRIGHTSHAPES\nAREA MEAN\nMAX AREA\nCOUNT 3\nAREA EVEN\nMAX VERTEXES\nMIN VERTEXES\nMIN AREA\nCOUNT ODD\n";
    std::istringstream com_iss(commands);

    while (com_iss.peek() != EOF)
    {
        std::string command;
        std::getline(com_iss, command);
        size_t semicolon_pos = command.find(' ');
        std::string command2 = command.substr(semicolon_pos + 1);
        command = command.substr(0, semicolon_pos);
        if (command == "AREA") {
            if (command2 == "ODD" || command2 == "EVEN") {
                std::cout << std::setprecision(1) << AREA(command2, polygons) << std::endl;
            }
            else if (command2 == "MEAN") {
                std::cout << std::setprecision(1) << AREA(polygons) << std::endl;
            }
            else if (std::all_of(command2.begin(), command2.end(), ::isdigit)) {
                std::cout << std::setprecision(1) << AREA(std::stoi(command2), polygons) << std::endl;
            }
            else {
                std::cout << " <INVALID COMMAND>\n";
            }
        }
        if (command == "MAX") {
            if (command2 == "AREA" || command2 == "VERTEXES") {
                std::cout << std::setprecision(1) << MAX(command2, polygons) << std::endl;
            }
            else {
                std::cout << " <INVALID COMMAND>\n";
            }
        }
        if (command == "MIN") {
            if (command2 == "AREA" || command2 == "VERTEXES") {
                std::cout << std::setprecision(1) << MIN(command2, polygons) << std::endl;
            }
            else {
                std::cout << " <INVALID COMMAND>\n";
            }
        }
        if (command == "COUNT") {
            if (command2 == "ODD" || command2 == "EVEN" || std::all_of(command2.begin(), command2.end(), ::isdigit)) {
                std::cout << std::setprecision(1) << COUNT(command2, polygons) << std::endl;
            }
            else {
                std::cout << " <INVALID COMMAND>\n";
            }
        }
        if (command == "RIGHTSHAPES") {
            std::cout << std::setprecision(1) << RIGHTSHAPES(polygons) << std::endl;
        }
        if (command == "PRINTENTER") {
            std::cout << std::setprecision(1) <<PRINTENTER(polygons) << std::endl;
        }
        if (command == "ECHO") {
            if (isRightFormat(command2)) {
                Polygon p;
                std::istringstream is(command2);
                is >> p;
                std::cout << ECHO(p, polygons) << std::endl;
            }
            else {
                std::cout << " <INVALID COMMAND>\n";
            }

        }


    }
    return 0;
}
