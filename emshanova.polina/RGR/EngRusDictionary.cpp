#include "EngRusDictionary.h"

bool EngRusDictionary::insertWord(const std::string& word, const std::string& translation)
{
    auto it = hashTable_.find(word);
    if (it == hashTable_.end())
    {
        std::set<std::string> listOfTranslations;
        listOfTranslations.insert( translation);
        return hashTable_.insert(std::make_pair(word, listOfTranslations)).second;
    }
    else
    {
        return it->second.insert(translation).second;
    }
}

bool EngRusDictionary::deleteWord(const std::string& word)
{
    return hashTable_.erase(word);
}

void EngRusDictionary::searchWord(const std::string& word) const
{
    auto it = hashTable_.find(word);
    if (it == hashTable_.end())
    {
        std::cout << "������ ����� ��� � �������\n";
    }
    else
    {
        auto translations = it->second;
        for (std::string translation : translations)
        {
            std::cout << translation << " ";
        }
        std::cout << '\n';
    }
}

void EngRusDictionary::printDictionary() const
{
    auto beg = hashTable_.begin();
    auto end = hashTable_.end();
    while (beg != end)
    {
        std::cout << beg->first << " - ";
        auto translations = beg->second;
        for (std::string translation : translations)
        {
            std::cout << translation << " ";
        }
        std::cout << '\n';
        beg++;
    }
}

